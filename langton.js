/*
étant donné une personne se déplaçant sur les cases noires et blanches d’une grille, et suivant pour cela deux règles très simples :

    Si la personne est sur une case noire, elle se tourne vers la gauche, change la couleur de la case en blanc et avance d’une case.
    Si la personne est sur une case blanche, elle se tourne vers la droite, change la couleur de la case en noir et avance d’une case.
*/
//directions
const DIRECTION = Object.freeze({
    GAUCHE: 0, DROITE: 1, BAS: 2, HAUT: 3
});
const SIZE_CELL = 5; //taille cellules
const ITEMS = 100; //taille canvas / taille cells
const DELAY = 3000; //delai en ms avant chaque deplacement
//tableau de cellules
var tabcells = [];
var personne = {};
var paused = false;
var iteration = 0;

/**
 * Switch pause
 */
function pause() {
  paused = !paused;
  if (!paused) {
    run();
  }
  document.getElementById("pause").innerHTML = (paused) ? "Rejouer" : "Pause";
}

/**
 * initialise le tableau de cellules et la taille du canvas
 * puis lance le traitement
 */
function init() {
  iteration = 0;
  personne = {
    d: DIRECTION.GAUCHE,
    x: Math.floor(ITEMS/2),
    y: Math.floor(ITEMS/2)
  };
  tabcells = Array.from({length: ITEMS}, _ => new Array(ITEMS).fill(0));
  const canvas = document.getElementById("matrice");
  canvas.width = ITEMS*SIZE_CELL;
  canvas.height = ITEMS*SIZE_CELL;
  run();
}

/**
 * Lance le tout
 */
function run(rCtx) {
  document.getElementById("iteration").innerHTML = ++iteration;
  let ctx = rCtx;
  //recupere le contexte ?
  if (!ctx) {
    const canvas = document.getElementById("matrice");
    if (canvas.getContext) {
      ctx = canvas.getContext("2d");
    }
  }
  //Fait le deplacement
  move(personne, ctx);
  //timer
  if (!paused) {
    setTimeout(() => {
        run(ctx);
      }, DELAY);
  }
}

/**
 * dessine un rectangle plein en blanc ou noir
 * @param {*} rCtx : contexte de dessin
 * @param {*} rX : position x
 * @param {*} rY : position y
 * @param {*} rFill : true pour remplir de noir, false sinon pour remplir de blanc
 */
function draw(rCtx, rX, rY, rFill) {
    rCtx.fillStyle = rFill ? "#000000" : "#ffffff"; //couleur ?
    rCtx.fillRect(rX, rY, SIZE_CELL, SIZE_CELL); //dessine carre
}

/**
 * Deplacement
 * @param {*} rPersonne : personne 
 * @param {*} rCtx : contexte de dessin
 */
function move(rPersonne, rCtx) {
  //Ici votre algo !
}
