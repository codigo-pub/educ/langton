# Petit programme pour un petit exercice

Voici un petit programme javascript qui utilise un objet **personne** et le fait se deplacer toutes les 'n' millisecondes.
Seulement le personnage se déplace selon des critères bien définis.

# Exercice 

Le but de l'exercice est de modifier le contenu de la fonction **move** pour qu'elle réponde aux règles de gestion suivantes : 

Etant donné une personne se déplaçant sur les cases noires et blanches d’une grille, et suivant pour cela deux règles très simples :

- Si la personne est sur une case noire, elle se tourne vers la gauche, change la couleur de la case en blanc et avance d’une case.
- Si la personne est sur une case blanche, elle se tourne vers la droite, change la couleur de la case en noir et avance d’une case.
- On estimera que la personne se déplace en direction de la gauche au début.

## Les constantes/variables
Le délai est configuré par défaut à 3000ms dans le ficheir javascript via la constante **DELAY**.

La méthode **draw** vous permettra de dessiner un carré plein noir ou blanc.

La variable **tabcells** correspond à la matrice carrée sur laquelle se déplace notre personnage. C'est un tableau à deux dimensions. On estimera que la première dimension c'est l'axe vertical (y) et la deuxième dimension l'axe horizontal (x). 

Une valeur 0 dans la matrice devra être interprétée comme cellule blanche, et 1 sera intérprété comme cellule noire.

La variable **personne** contient 3 informations sur l'état du personnage : 
- d : la direction (c'est une énumeration -> **DIRECTION**)
- x : représente la position sur l'axe des x
- y : représente la position sur l'axe des y

## Exemple avec vitesse (DELAY) à 1

<img src="exemple.gif" style="float: left; margin-right: 10px;" />